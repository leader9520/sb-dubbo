package com.pccw.core.base;

import org.springframework.stereotype.Service;

@Service
public interface BaseService<T> {
	T findById(String id);

	T findByName(String name);
}
