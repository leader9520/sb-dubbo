package com.pccw.core.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseDao<T> extends JpaRepository<T, String> {

	T findById(String id);

	T findByName(String name);

}
