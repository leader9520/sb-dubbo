package com.pccw.sb.dubbo.service;

import com.pccw.sb.dubbo.entity.User;

public interface UserService {

	public User login(User user);

}
