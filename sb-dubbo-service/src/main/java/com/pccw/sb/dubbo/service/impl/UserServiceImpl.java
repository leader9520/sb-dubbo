package com.pccw.sb.dubbo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pccw.sb.dubbo.dao.UserDao;
import com.pccw.sb.dubbo.dao.UserLogDao;
import com.pccw.sb.dubbo.entity.User;
import com.pccw.sb.dubbo.entity.UserLog;
import com.pccw.sb.dubbo.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;
	@Autowired
	UserLogDao userLogDao;

	@Override
	@Transactional
	public User login(User user) {
		UserLog log = new UserLog();
		log.setFlag(0);
		log.setName(user.getName());
		// ExampleMatcher matcher =
		// ExampleMatcher.matching().withIgnorePaths("id");

		// Example<User> example = Example.of(user, matcher);
		// Example<User> example = Example.of(user);
		// User result = userDao.findOne(example);
		User result = userDao.findByName(user.getName());
		if (result != null) {
			log.setFlag(1);
		}
		userLogDao.save(log);

		return result;
	}

}
