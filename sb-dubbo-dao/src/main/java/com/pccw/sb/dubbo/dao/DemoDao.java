package com.pccw.sb.dubbo.dao;

import java.util.List;

import com.pccw.sb.dubbo.entity.Demo;

public interface DemoDao {
	public Integer addDemo(String name);

	public Demo getById(Integer id);

	public List<Demo> getList(String name);
}
