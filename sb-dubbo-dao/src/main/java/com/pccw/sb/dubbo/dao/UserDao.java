package com.pccw.sb.dubbo.dao;

import java.util.List;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pccw.core.base.BaseDao;
import com.pccw.sb.dubbo.entity.User;

@Repository
@CacheConfig(cacheNames = "user")
public interface UserDao extends BaseDao<User> {

	User findByName(String name);

	User findByNameAndAge(String name, Integer age);

	@Query("from User u where u.name=:name")
	User findUser(@Param("name") String name);
	
	@Override
	@Cacheable
	List<User> findAll();
}
