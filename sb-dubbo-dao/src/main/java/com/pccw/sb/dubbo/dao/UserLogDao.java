package com.pccw.sb.dubbo.dao;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Repository;

import com.pccw.core.base.BaseDao;
import com.pccw.sb.dubbo.entity.UserLog;

@Repository
@CacheConfig(cacheNames = "userLog")
public interface UserLogDao extends BaseDao<UserLog> {

}
