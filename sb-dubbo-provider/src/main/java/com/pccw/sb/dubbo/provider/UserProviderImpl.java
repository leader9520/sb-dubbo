package com.pccw.sb.dubbo.provider;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.common.json.JSON;
import com.alibaba.dubbo.config.annotation.Service;
import com.pccw.sb.dubbo.api.UserProvider;
import com.pccw.sb.dubbo.dao.UserDao;
import com.pccw.sb.dubbo.entity.User;
import com.pccw.sb.dubbo.service.UserService;

//注册为 Dubbo 服务
@Service(version = "1.0.0")
public class UserProviderImpl implements UserProvider {

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserService userService;

	@Override
	public User findByName(String name) {
		// TODO Auto-generated method stub
		User user = userDao.findByName(name);
		try {
			System.out.println("========= " + JSON.json(user));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User findByNameAndAge(String name, Integer age) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(User entity) {
		userDao.save(entity);
	}

	@Override
	public List<User> saveList(List<User> entities) {
		return userDao.save(entities);
	}

	@Override
	public List<User> findAll() {
		return userDao.findAll();
	}

	@Override
	public User login(User user) {
		return userService.login(user);
	}

}
