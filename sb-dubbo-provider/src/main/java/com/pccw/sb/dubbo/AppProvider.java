/**
 * @author Dong-Quan.Li@pccw.com
 * @date 2017年8月16日 下午12:15:01
 */
package com.pccw.sb.dubbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author mervin
 *
 */

@Configuration
@EnableAutoConfiguration
@ComponentScan("com.pccw")
@EntityScan(basePackages = "com.pccw.**.entity")
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.pccw.**.dao")
@EnableCaching
@SpringBootApplication
public class AppProvider {

	public static void main(String[] args) {
		SpringApplication.run(AppProvider.class, args);
	}

}
