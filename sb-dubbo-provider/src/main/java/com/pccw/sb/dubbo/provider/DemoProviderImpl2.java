/**
 * @author Dong-Quan.Li@pccw.com
 * @date 2017年8月16日 下午2:18:57
 */
package com.pccw.sb.dubbo.provider;

import java.util.Date;
import java.util.List;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;

import com.alibaba.dubbo.config.annotation.Service;
import com.pccw.sb.dubbo.api.DemoProvider;
import com.pccw.sb.dubbo.entity.Demo;

/**
 * @author mervin
 *
 */
// 注册为 Dubbo 服务
@Service(version = "1.2.0")
//@CacheConfig(cacheNames = "demo")
public class DemoProviderImpl2 implements DemoProvider {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pccw.sb.dubbo.api.DemoProvider#addDemo(java.lang.String)
	 */
	@Override
	public Integer addDemo(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pccw.sb.dubbo.api.DemoProvider#getById(java.lang.Integer)
	 */
	// @Cacheable("userCache")
//	@Cacheable
	@Override
	public Demo getById(Integer id) {
		System.out.println("进来服务了1.2.0...");
		Demo demo = new Demo();
		demo.setId(id);
		demo.setName("demo" + id);
		demo.setCreateTime(new Date());
		return demo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pccw.sb.dubbo.api.DemoProvider#getList(java.lang.String)
	 */
	@Override
	public List<Demo> getList(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
