package com.pccw.sb.dubbo.web.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.dubbo.common.json.JSON;
import com.alibaba.dubbo.config.annotation.Reference;
import com.pccw.core.base.BaseController;
import com.pccw.sb.dubbo.api.UserProvider;
import com.pccw.sb.dubbo.entity.User;

/**
 * Created by gs on 2017/5/19 0019.
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

	@Reference(version = "1.0.0")
	private UserProvider userService;

	@RequestMapping("/login")
	public User login(String name, String pwd) {
		System.out.println("name= " + name + "\tpwd=" + pwd);
		User user = new User();
		user.setName(name);
		user = userService.login(user);
		return user;
	}

	@RequestMapping("/get")
	public List<User> getDemo(String name) {
		System.out.println("name= " + name);

		// 创建10条记录
		for (int i = 0; i < 10; i++) {
			// userService.save(new User(name + i, (i + 1) * 10));
		}

		List<User> userList = userService.findAll();
		try {
			System.out.println(JSON.json(userList));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userList;
	}

	@RequestMapping("/get2")
	public List<User> getDemo2() {
		System.out.println("list= " + null);
		// int age = 30;
		// Order orders = new Order(Direction.DESC, "age");

		// Sort sort = new Sort(orders);

		// Pageable page = new PageRequest(0, 10, sort);
		// return userService.queryFamilyList(age, page);
		return null;
	}
}
