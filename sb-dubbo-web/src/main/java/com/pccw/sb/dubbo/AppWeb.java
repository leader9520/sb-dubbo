/**
 * @author Dong-Quan.Li@pccw.com
 * @date 2017年8月16日 下午2:33:26
 */
package com.pccw.sb.dubbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

/**
 * @author mervin
 *
 */
// @SpringBootApplication
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
public class AppWeb {
	public static void main(String[] args) {
		SpringApplication.run(AppWeb.class, args);
	}
}
