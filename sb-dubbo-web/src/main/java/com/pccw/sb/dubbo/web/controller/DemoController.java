package com.pccw.sb.dubbo.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pccw.sb.dubbo.api.DemoProvider;
import com.pccw.sb.dubbo.entity.Demo;

/**
 * Created by gs on 2017/5/19 0019.
 */
@RestController
public class DemoController {

	@Reference(version = "1.0.0")
	private DemoProvider demoProvider;
	@Reference(version = "1.2.0")
	private DemoProvider demoProvider2;

	@RequestMapping("/get")
	public Demo getDemo(Integer id) {
		System.out.println("id= " + id);
		return demoProvider.getById(id);
	}
	
	@RequestMapping("/get2")
	public Demo getDemo2(Integer id) {
		System.out.println("id= " + id);
		return demoProvider2.getById(id);
	}
}
