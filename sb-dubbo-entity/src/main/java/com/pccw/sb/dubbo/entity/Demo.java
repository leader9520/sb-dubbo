/**
 * @author Dong-Quan.Li@pccw.com
 * @date 2017年8月16日 上午11:57:29
 */
package com.pccw.sb.dubbo.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author mervin
 *
 */
public class Demo implements Serializable {
	private Integer id;
	private String name;
	private Date createTime;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
