package com.pccw.sb.dubbo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.pccw.core.base.BaseEntity;

@Entity
public class UserLog extends BaseEntity {

	public UserLog() {
		// TODO Auto-generated constructor stub
	}

	public UserLog(String name, Integer flag) {
		this.setName(name);
		this.setFlag(flag);
	}

	@Id
	@GeneratedValue
	private Long id;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private Integer flag;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

}
