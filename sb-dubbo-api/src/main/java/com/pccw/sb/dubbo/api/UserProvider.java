package com.pccw.sb.dubbo.api;

import java.util.List;

import com.pccw.sb.dubbo.entity.User;

public interface UserProvider {

	User findByName(String name);

	User findByNameAndAge(String name, Integer age);

	void save(User user);

	List<User> findAll();

	List<User> saveList(List<User> entities);

	User login(User user);
}
