/**
 * @author Dong-Quan.Li@pccw.com
 * @date 2017年8月16日 下午12:06:00
 */
package com.pccw.sb.dubbo.api;

import java.util.List;

import com.pccw.sb.dubbo.entity.Demo;

/**
 * @author mervin
 *
 */

public interface DemoProvider {

	public Integer addDemo(String name);

	public Demo getById(Integer id);

	public List<Demo> getList(String name);
}
